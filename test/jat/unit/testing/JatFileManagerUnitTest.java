package jat.unit.testing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import jat.app.bussines.JatFilesManager;
import jat.app.bussines.OSInfo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alcidesticlla
 */
public class JatFileManagerUnitTest {
    private static   JatFilesManager jatFilesManager;
    
    // estas variables deber ser configuradas con la direccion de jat-framework dependiendo del SO y la pc o laptop
    private static String rootPathWin = "C:\\Users\\Alcides\\Documents\\JAT\\jat-framework";
    private static String rootPathMac = "/Users/alcidesticlla/JAT-SW1/JATFramework-Template";
    private static String rootPathLin = "/home/osboxes/JAT/jat-framework";
    
    // Varialbe a ser usada como ruta de archivo, una de las de arriba dependiendo el so
    private static String rootPath = "";
    
    @BeforeClass
    public static void JatManagerTestConstructor() {
        
        if (OSInfo.isLinux()){
            rootPath = rootPathLin;
        }else if(OSInfo.isMac()){
            rootPath = rootPathMac;
        }else if (OSInfo.isWindows()){
            rootPath = rootPathWin;
        }
        
        jatFilesManager=new JatFilesManager(rootPath);
        assertNotNull(jatFilesManager);
        System.out.println("hola mundo +)"+jatFilesManager);
    }
    
    
    @Test
    public void notNullConstructorTest(){
        assertNotNull(jatFilesManager);
    }
    
    @Test
    public void rootPathTest(){
        assertEquals(rootPath,jatFilesManager.getRootPath()); 
    }
    
    @Test
    public void createJATText() {
        jatFilesManager.createJATText();
        String configfile = jatFilesManager.readConfigFile();
        assertTrue(configfile.contains(JatFilesManager.Factory_Link));
        assertTrue(configfile.contains(JatFilesManager.PO_Link));
        assertTrue(configfile.contains(JatFilesManager.Test_Link));
    }
    
    @Test
    public void rewriterTest(){
        jatFilesManager.reWriteConfigFile(JatFilesManager.Factory_Link, JatFilesManager.PO_Link,JatFilesManager.Test_Link);
        String configfile = jatFilesManager.readConfigFile();
        assertTrue(configfile.contains(JatFilesManager.Factory_Link));
        assertTrue(configfile.contains(JatFilesManager.PO_Link));
        assertTrue(configfile.contains(JatFilesManager.Test_Link));
    }
    
    @Test 
    public void writeNewLogTest(){
        jatFilesManager.writeNewLog(rootPath);
        assertTrue(jatFilesManager.readLog(jatFilesManager.getLenght()).contains(rootPath));
    }
    
    @Test
    public void readnewLog(){
        int index = jatFilesManager.getLenght();
        jatFilesManager.writeNewLog(rootPath); 
        assertTrue(index < jatFilesManager.getLenght());
        assertTrue(jatFilesManager.readLog(jatFilesManager.getLenght()).contains(rootPath));
    }
    
    @AfterClass
    public static void estadoInicial(){
        jatFilesManager.clearLog();
        jatFilesManager.clearJat();
    }
 
}
