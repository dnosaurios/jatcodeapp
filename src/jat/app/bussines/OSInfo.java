/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jat.app.bussines;

/**
 *
 * @author alcidesticlla
 */
public class OSInfo {

    private static String OS = System.getProperty("os.name").toLowerCase();

    public static boolean isMac() {
        return (OS.indexOf("mac") >= 0);
    }

    public static boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }
    
    public static boolean isLinux(){
        return (OS.indexOf("linux") >= 0);
    }

    public static boolean isUnix() {
        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
    }

    public static boolean isSolaris() {
        return (OS.indexOf("sunos") >= 0);
    }

    public static String getOS() {
        if (isWindows()) {
            return "win";
        } else if (isMac()) {
            return "osx";
        } else if (isUnix()) {
            return "uni";
        } else if (isSolaris()) {
            return "sol";
        } else {
            return "err";
        }
    }
}
