JATCodeApp
==========

JWAT - Java Web Automator Tool es una herramienta que permite facilitar el proceso de automatizacion de paginas web.
los diferentes script que la app genera deben ser alojados en [JAT-Framework](https://bitbucket.org/dnosaurios/jat-framework/overview) el cual es un entorno de ejecucion para las pruebas que van ha ser generadas.

# Prerequisitos #
**1** Instalar Java

**2** Instalar Eclipse.

**3** Configurar [JAT-Framework](https://bitbucket.org/dnosaurios/jat-framework/overview)

# Instalacion #

**1** Clona el repositorio

```
#!bash
git clone https://alcidesticlla@bitbucket.org/dnosaurios/jatcodeapp.git

```
**2** Importar el proyecto en Net Beans

**3** Ejecutar el proyecto

# Sanity Check #